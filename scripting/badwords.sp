#include <sourcemod>

#define PLUGIN_NAME "Bad Words Filter"
#define PLUGIN_AUTHOR "Cr1MsOn"
#define PLUGIN_DESCRIPTION "Blocks the bad words on chat"
#define PLUGIN_VERSION "1.0"
#define PLUGIN_URL "No website for now"


new String:Bad_Words[50][30];


public Plugin:myinfo = {
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
}


public OnPluginStart() {
	RegConsoleCmd("say", Block_Bad_Words);
	RegConsoleCmd("say_team", Block_Bad_Words);
	ReadBadWords();

}

public Array(const String:Line[]) {
	if(strlen(Line) > 1) {
		for (new i = 0; i < 50; i++) {
			if(strlen(Bad_Words[i]) < 1 && !StrEqual(Bad_Words[i], Line)) {
				strcopy(Bad_Words[i], 30, Line);
			}
		}
	}
}

public ReadBadWords() {
	new String:g_filename[PLATFORM_MAX_PATH]
	BuildPath(Path_SM, g_filename, sizeof(g_filename), "configs/badwords.ini");

	new Handle:file = OpenFile(g_filename, "rt");

	if (file == INVALID_HANDLE) {
		PrintToServer("[BadWords] Could not open file!");
		return;
	}

	while (!IsEndOfFile(file)) {
		decl String:line[255];

		if(!ReadFileLine(file, line, sizeof(line))) {
			break;
		}

		if ((line[0] == '/' && line[1] == '/') || (line[0] == ';' || line[0] == '\0')) {
			continue;
		}

		Array(line);
	}

	CloseHandle(file);
} 


stock Active_Badword(String:command[]) {
	new badword = 0;
	new word_index = 0;

	while ((badword == 0) && (word_index < sizeof(Bad_Words))) {
		if (StrContains(command, Bad_Words[word_index], false) > -1) {
			badword++;
		}
		word_index++;
	}

	if (badword > 0) {
		return 1;
	}
	return 0;
}

public Action:Block_Bad_Words(client, args) {
	if (client) {
		decl String:user_command[192];
		GetCmdArgString(user_command, 192);

		new start_index = 0;
		new command_length = strlen(user_command);
		if (command_length > 0) {
			if (user_command[0] == 34) {
				start_index = 1;
				if (user_command[command_length - 1] == 34) {
					user_command[command_length - 1] = 0;
				}
			}

			if (user_command[start_index] == 47) {
				start_index++;
			}

			new command_block = Active_Badword(user_command[start_index]);
			if (command_block > 0) {

				if ((!IsFakeClient(client)) && (IsClientConnected(client))) {
					PrintToChat(client, "[\x07BadWord\x01] Dont talk like this please");
				}

				return Plugin_Handled;
			}
		}
	}
	return Plugin_Continue;
}

